package ru.nirinarkhova.tm.Constant;

public class TerminalConst {
    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
}
